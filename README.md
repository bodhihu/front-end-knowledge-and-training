# Frontend Knowledge and Training

> A knowledge collection and guides for a frontend engineer


- [Frontend Knowledge and Training](#frontend-knowledge-and-training)
  - [Knowledge Map](#knowledge-map)
    - [Overall view](#overall-view)
    - [Engineering Stacks](#engineering-stacks)
    - [JavaScript](#javascript)
    - [TypeScript](#typescript)
    - [NodeJs](#nodejs)
    - [React](#react)
    - [Vue](#vue)
    - [Angular](#angular)
    - [Hybrid](#hybrid)
      - [Ionic Framework / Cordova](#ionic-framework--cordova)
      - [ReactNative](#reactnative)
      - [flutter](#flutter)
  - [Start Learning the Web Fundamentals](#start-learning-the-web-fundamentals)

Also see knowledge maps of:<br/>
* [Server](./Server/readme.md)
  * Java / Spring
  * Nodejs / Nestjs, Eggjs, ...
* [DB](./DB/readme.md)
  * SQL

## Knowledge Map

### Overall view

<img src="./imgs/前端主流技术栈.jpeg" />

### Engineering Stacks

<img src="./imgs/前端工程化体系.jpeg" />

### JavaScript
<img src="./imgs/js.png" />

### TypeScript
<img src="./imgs/ts.png" />

### NodeJs

<img src="./imgs/nodejs.jpeg" />

### React

<img src="./imgs/reactjs-stack.png" />

### Vue
<img src="./imgs/vuejs-stack.png" />

### Angular
<img src="./imgs/angularjs-stack.png" />

### Hybrid
<img src="./imgs/前端跨平台.png" />

#### Ionic Framework / Cordova
<img src="./imgs/ionic.png" />
<img src="./imgs/cordova.png" />

#### ReactNative
<img src="./imgs/react-native.png" />

#### flutter
<img src="./imgs/flutter.png" />

## Start Learning the Web Fundamentals

To learn the web fundamentals, we can goto the [W3Schools](https://www.w3schools.com) site which had very good courses on the web fundamental technologies (HTML/CSS/JS/NodeJS/Java/SQL, and many more...)

• [HTML](https://www.w3schools.com/html/default.asp)
• [CSS](https://www.w3schools.com/css/default.asp)
• [JavaScript](https://www.w3schools.com/js/default.asp)
• [Sass](https://www.w3schools.com/sass/default.php)
• [jQuery](https://www.w3schools.com/jquery/default.asp)
• [NodeJs](https://www.w3schools.com/nodejs/default.asp)
• [MySQL](https://www.w3schools.com/mysql/default.asp)
• [React](https://www.w3schools.com/react/default.asp)
• [Python](https://www.w3schools.com/python/default.asp)
• ...

<br/>

**To practise and test yourself:**

[Exercises](https://www.w3schools.com/exercises/index.php)
